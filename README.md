<h1 align="center">Object Spy</h1>

<p align="center">
    <strong>Debugging library used to spy on objects' private values/methods</strong>
</p>

<!--
TODO: Make sure the following URLs are correct and working for your project.
      Then, remove these comments to display the badges, giving users a quick
      overview of your package.

<p align="center">
    <a href="https://packagist.org/packages/danjones000/object-spy"><img src="https://img.shields.io/packagist/v/danjones000/object-spy.svg?style=flat-square&label=release" alt="Download Package"></a>
    <a href="https://php.net"><img src="https://img.shields.io/packagist/php-v/danjones000/object-spy.svg?style=flat-square&colorB=%238892BF" alt="PHP Programming Language"></a>
    <a href="https://gitlab.com/danjones000/object-spy/blob/main/LICENSE"><img src="https://img.shields.io/packagist/l/danjones000/object-spy.svg?style=flat-square&colorB=darkcyan" alt="Read License"></a>
    <a href="https://github.com/danjones000/object-spy/actions/workflows/continuous-integration.yml"><img src="https://img.shields.io/github/workflow/status/danjones000/object-spy/build/main?style=flat-square&logo=github" alt="Build Status"></a>
    <a href="https://codecov.io/gh/danjones000/object-spy"><img src="https://img.shields.io/codecov/c/gh/danjones000/object-spy?label=codecov&logo=codecov&style=flat-square" alt="Codecov Code Coverage"></a>
    <a href="https://shepherd.dev/gitlab/danjones000/object-spy"><img src="https://img.shields.io/endpoint?style=flat-square&url=https%3A%2F%2Fshepherd.dev%2Fgitlab%2Fdanjones000%2Fobject-spy%2Fcoverage" alt="Psalm Type Coverage"></a>
</p>
-->


## About

A debugging/testing tool which allows access to an object's or class's private
and protected properties and methods without the use of Reflection.

This project adheres to a [code of conduct](CODE_OF_CONDUCT.md).
By participating in this project and its community, you are expected to
uphold this code.


## Installation

Install this package as a dependency using [Composer](https://getcomposer.org).

``` bash
composer require danjones000/object-spy
```

## Usage

``` php
use Danjones000\Spy\ObjectProxy;

$object = new Object(); // Can be any object.

$spy = new ObjectProxy($object);

// Access private property
echo $spy->privateProperty;

// Set private property
$spy->privateProperty = 42;

// Call private method
$spy->privateMethod();

// Call private method with parameters
$spy->protectedMethod(42, false);

// Run arbitry code on object, allows passing in parameters
// All arguments after the closure are passed as arguments to the closure itself
$spy->call(fn ($abc) => $this->someProp = $this->someMethod($abc) * static::MULTIPLIER, 500);

// PHP allows static methods to be called non-statically, so, this works as well
$spy->staticPrivateMethod();

// Can also access static properties/methods without an object instance
$staticSpy = new ObjectProxy(Object::class);

// If there is an Object::$privateStaticProperty
echo $staticSpy->privateStaticProperty;
$staticSpy->privateStaticProperty = 42;

$staticSpy->privateStaticMethod();

// No direct access to contstants, but we can do this
$staticSpy->call(fn () => static::PRIVATE_CONSTANT); // self also works, of course
```

## Implementation notes

This class does not use reflections in its operation. Instead, if uses closures
that are bound to the instance or class.

## Contributing

Contributions are welcome! To contribute, please familiarize yourself with
[CONTRIBUTING.md](CONTRIBUTING.md).

## Copyright and License

The danjones000/object-spy library is copyright © [Dan Jones](https://danielrayjones.com/)
and licensed for use under the terms of the
MIT License (MIT). Please see [LICENSE](LICENSE) for more information.
