<?php

/**
 * This file is part of danjones000/object-spy
 *
 * danjones000/object-spy is open source software: you can distribute
 * it and/or modify it under the terms of the MIT License
 * (the "License"). You may not use this file except in
 * compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * permissions and limitations under the License.
 *
 * @copyright Copyright (c) Dan Jones <danjones@goodevilgenius.org>
 * @license https://opensource.org/licenses/MIT MIT License
 */

declare(strict_types=1);

namespace Danjones000\Spy;

/**
 * @param string|object $classOrObject
 *
 * @psalm-suppress RedundantConditionGivenDocblockType
 */
function spy($classOrObject): ObjectProxy
{
    return new ObjectProxy($classOrObject);
}
