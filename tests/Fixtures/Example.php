<?php

declare(strict_types=1);

namespace Danjones000\Test\Spy\Fixtures;

class Example
{
    protected const PROT_CONST = 2;
    private const PRIV_CONST = 3;
    protected static int $protStatic = 4;
    private static int $privStatic = 5;
    protected int $prot = 6;
    private int $priv = 7;

    public static function reset(): void
    {
        static::$protStatic = 4;
        static::$privStatic = 5;
    }

    protected function protMethod(): int
    {
        return $this->prot;
    }

    private function privMethod(int $priv): void
    {
        $this->priv = $priv;
    }

    protected static function protStatMethod(int $protStatic): void
    {
        static::$protStatic = $protStatic;
    }

    private static function privStatMethod(): int
    {
        return static::$privStatic;
    }
}
