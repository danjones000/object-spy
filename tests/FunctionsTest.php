<?php

declare(strict_types=1);

namespace Danjones000\Test\Spy;

use Danjones000\Spy\ObjectProxy;

use function Danjones000\Spy\spy;

class FunctionsTest extends TestCase
{
    public function testSpy(): void
    {
        $spy = spy(Fixtures\Example::class);
        $this->assertInstanceOf(ObjectProxy::class, $spy);
    }
}
