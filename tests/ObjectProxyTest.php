<?php

declare(strict_types=1);

namespace Danjones000\Test\Spy;

use Danjones000\Spy\ObjectProxy;

class ObjectProxyTest extends TestCase
{
    protected Fixtures\Example $example;
    protected ObjectProxy $spy;
    protected ObjectProxy $classSpy;
    protected \ReflectionObject $reflection;

    protected function setUp(): void
    {
        parent::setUp();
        Fixtures\Example::reset();
        $this->example = new Fixtures\Example();
        $this->spy = new ObjectProxy($this->example);
        $this->classSpy = new ObjectProxy(Fixtures\Example::class);
        $this->reflection = new \ReflectionObject($this->example);
    }

    public function testCallingProtectedMethod(): void
    {
        $ret = $this->spy->protMethod();
        $this->assertEquals(6, $ret);
    }

    public function testCallPrivateMethod(): void
    {
        $this->spy->privMethod(42);
        $prop = $this->reflection->getProperty('priv');
        $prop->setAccessible(true);

        $this->assertEquals(42, $prop->getValue($this->example));
    }

    public function testCallProtectedStaticMethod(): void
    {
        $this->classSpy->protStatMethod(42);
        $prop = $this->reflection->getProperty('protStatic');
        $prop->setAccessible(true);

        $this->assertEquals(42, $prop->getValue());
    }

    public function testCallPrivateStaticMethod(): void
    {
        $ret = $this->classSpy->privStatMethod();
        $this->assertEquals(5, $ret);
    }

    public function testAccessPrivateProperty(): void
    {
        $this->assertEquals(7, $this->spy->priv);
    }

    public function testAccessProtectedProperty(): void
    {
        $this->assertEquals(6, $this->spy->prot);
    }

    public function testAccessPrivateStaticProperty(): void
    {
        $this->assertEquals(5, $this->classSpy->privStatic);
    }

    public function testAccessProtectedStaticProperty(): void
    {
        $this->assertEquals(4, $this->classSpy->protStatic);
    }

    public function testAccessPrivateConstantThroughCall(): void
    {
        $ret = $this->classSpy->call(fn () => static::PRIV_CONST);
        $this->assertEquals(3, $ret);
        $ret = $this->classSpy->call(fn () => self::PRIV_CONST);
        $this->assertEquals(3, $ret);
    }

    public function testAccessProtectedConstantThroughCall(): void
    {
        $ret = $this->classSpy->call(fn () => static::PROT_CONST);
        $this->assertEquals(2, $ret);
        $ret = $this->classSpy->call(fn () => self::PROT_CONST);
        $this->assertEquals(2, $ret);
    }

    public function testModifyPrivateProperty(): void
    {
        $this->spy->priv = 42;
        $prop = $this->reflection->getProperty('priv');
        $prop->setAccessible(true);

        $this->assertEquals(42, $prop->getValue($this->example));
    }

    public function testModifyProtectedProperty(): void
    {
        $this->spy->prot = 42;
        $prop = $this->reflection->getProperty('prot');
        $prop->setAccessible(true);

        $this->assertEquals(42, $prop->getValue($this->example));
    }

    public function testModifyPrivateStaticProperty(): void
    {
        $this->classSpy->privStatic = 42;
        $prop = $this->reflection->getProperty('privStatic');
        $prop->setAccessible(true);

        $this->assertEquals(42, $prop->getValue());
    }

    public function testModifyProtectedStaticProperty(): void
    {
        $this->classSpy->protStatic = 42;
        $prop = $this->reflection->getProperty('protStatic');
        $prop->setAccessible(true);

        $this->assertEquals(42, $prop->getValue());
    }
}
